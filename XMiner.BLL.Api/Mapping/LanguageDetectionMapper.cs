﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Models.Response;

namespace XMiner.BLL.Api.Mapping
{
    public static class LanguageDetectionMapper
    {
        public static LanguageDetectionResponse MapToRepoResponse(this Language model)
        {
            if (model == null) return null;

            return new LanguageDetectionResponse()
            {
                Text = model.Text,
                Language = model.Lang,
                Confidence = model.Confidence
            };
        }

        public static async Task<LanguageDetectionResponse> MapToRepoResponseAsync(this Language model)
        {
            if (model == null) return null;

            var task = Task.Run(() => model.MapToRepoResponse());
            return await task;
        }
    }
}
