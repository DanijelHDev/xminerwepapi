﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Common.Helpers;
using XMiner.Core.Entity.Entities;

namespace XMiner.BLL.Api.Mapping
{
    public static class AccountTextMapper
    {
        public static AccountText MapToModel(this AccountTextRequest request)
        {
            if (request == null) return null;

            return new AccountText
            {
                Id = Guid.NewGuid(),
                Text = request.Text,
                Headline = request.Headline,
                SentenceNumber = request.SentenceNumber,
                MiningType = request.TextMiningType,
                IsPrivate = request.IsPrivate,
                AccountId = CurrentAccount.Id,
                DateModified = DateTime.UtcNow,
                DateCreated = DateTime.UtcNow,
                ModifiedBy = CurrentAccount.UserName,
                CreatedBy = CurrentAccount.UserName
            };
        }

        public static async Task<AccountText> MapToModelAsync(this AccountTextRequest request)
        {
            if (request == null) return null;

            var task = Task.Run(() => request.MapToModel());
            return await task;
        }
    }
}
