﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Models.Response;
using XMiner.Core.Entity.Entities;

namespace XMiner.BLL.Api.Mapping
{
    public static class EntityMapper
    {
        public static EntityResponse MapToResponse(this Entity entities, AccountText textModel, string text)
        {
            if (entities == null) return null;

            var response = new EntityResponse();
            response.Text = text;
            response.Id = textModel?.Id.ToString();
            response.AccountDisplayName = textModel != null ? $"{textModel?.Account.FirstName} {textModel?.Account.LastName}" : null;
            response.MiningType = textModel?.MiningType;
            response.Entities.Location = entities?.Location?.ToList() ?? new List<string>();
            response.Entities.Keyword = entities?.Keyword?.ToList() ?? new List<string>();
            response.Entities.Organization = entities?.Organization?.ToList() ?? new List<string>();
            response.Entities.Person = entities?.Person?.ToList() ?? new List<string>();
            response.Entities.Date = entities?.Date?.ToList() ?? new List<string>();
            response.Entities.Time = entities?.Time?.ToList() ?? new List<string>();
            response.Entities.Phone = entities?.Phone?.ToList() ?? new List<string>();

            return response;
        }

        public static async Task<EntityResponse> MapToResponseAsync(this Entity entities, AccountText textModel, string text)
        {
            if (entities == null) return null;

            var task = Task.Run(() => entities.MapToResponse(textModel, text));
            return await task;
        }
    }
}
