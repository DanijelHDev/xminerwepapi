﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Models.Response;
using XMiner.Core.Entity.Entities;

namespace XMiner.BLL.Api.Mapping
{
    public static class HashTagsMapper
    {
        public static HashtagsExtractionResponse MapToResponse(this Hashtags model, AccountText textModel, string text)
        {
            if (model == null) return null;

            return new HashtagsExtractionResponse()
            {
                Id = textModel?.Id.ToString(),
                Text = text,
                AccountDisplayName = textModel != null ? $"{textModel?.Account.FirstName} {textModel?.Account.LastName}" : null,
                MiningType = textModel?.MiningType,
                Language = model?.Language ?? String.Empty,
                Hashtags = model?.HashtagsMember?.ToList() ?? new List<string>()
            };
        }

        public static async Task<HashtagsExtractionResponse> MapToResponseAsync(this Hashtags model, AccountText textModel, string text)
        {
            if (model == null) return null;

            var task = Task.Run(() => model.MapToResponse(textModel, text));
            return await task;
        }
    }
}
