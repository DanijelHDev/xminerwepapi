﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;

namespace XMiner.BLL.Api.Services
{
    public interface IAccountTextService
    {
        Task<bool> SaveText(AccountTextRequest request);
        Task<AccountTextResponse> GetTexts();
        Task<AccountTextResponse> GetText(Guid id);
        Task<AccountTextResponse> GetAllPublicTexts();
        Task<AccountTextResponse> GetTextsEntities();
        Task<bool> SetAccountTextAccess(Guid Id);

        Task<bool> DeleteText(Guid Id);
        
    }
}
