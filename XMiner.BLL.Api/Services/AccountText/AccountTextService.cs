﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Mapping;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;
using XMiner.BLL.Common.Helpers;
using XMiner.Core.DataAccess.UnitOfWork;
using XMiner.Core.Entity.Entities;

namespace XMiner.BLL.Api.Services
{
    public class AccountTextService : IAccountTextService
    {
        private AccountTextResponse _response;
        private readonly IUnitOfWork _unitOfWork;

        public AccountTextService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// SAVES ACCOUNT TEXT IN DB
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> SaveText(AccountTextRequest request)
        {
            //SUMMARIZATION HAS REQUIRED HEADLINE & SENTENCE COUNT
            if (request.TextMiningType == ((int)Enumerators.AccountTextMiningType.Summarization))
            {
                if (String.IsNullOrEmpty(request.Headline) || request.SentenceNumber == null)
                    return false;
            }

            var accountText = await request.MapToModelAsync();

            _unitOfWork.AccountTexts.AddToDatabase(accountText);
            if (await _unitOfWork.ExecuteUpdatesAsync() > 0)
                return true;

            return false;
        }

        /// <summary>
        /// GET SINGLE TEXT AND IT'S MINING
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AccountTextResponse> GetText(Guid id)
        {
            var text = _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Include(a => a.Account)
                .FirstOrDefault(a => a.Id.Equals(id));

            if (text == null)
                return null;

            _response = new AccountTextResponse();
            await HandleTexts(text);

            if (_response.EntityTexts.Count == 0)
                _response.EntityTexts = null;

            if (_response.HashtagsTexts.Count == 0)
                _response.HashtagsTexts = null;

            if (_response.SummarizationTexts.Count == 0)
                _response.SummarizationTexts = null;

            return _response;
        }

        public async Task<AccountTextResponse> GetAllPublicTexts()
        {
            var texts = await _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Include(a => a.Account)
                .Where(a => a.IsPrivate.Equals(false))
                .AsNoTracking().ToListAsync();

            _response = new AccountTextResponse();

            var tasks = texts.Select(t => HandleTexts(t));
            await Task.WhenAll(tasks);

            return _response;
        }

        /// <summary>
        /// GET ALL TEXTS FOR ACCOUNT
        /// </summary>
        /// <returns></returns>
        public async Task<AccountTextResponse> GetTexts()
        {
            var texts = await _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Where(a => a.AccountId.Equals(CurrentAccount.Id))
                .AsNoTracking().ToListAsync();

            _response = new AccountTextResponse();
            var tasks = texts.Select(t => HandleTexts(t));
            await Task.WhenAll(tasks);
            
            return _response;
        }

        public async Task<AccountTextResponse> GetTextsEntities()
        {
            var texts = await _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Where(a => a.AccountId.Equals(CurrentAccount.Id))
                .Where(a => a.MiningType.Equals((int)Enumerators.AccountTextMiningType.Entities))
                .AsNoTracking().ToListAsync();

            _response = new AccountTextResponse();
            var tasks = texts.Select(t => HandleTexts(t));
            await Task.WhenAll(tasks);

            //IGNORE JUST FOR STUBAN xD
            _response.HashtagsTexts = null;
            _response.SummarizationTexts = null;

            return _response;
        }

        public async Task<bool> SetAccountTextAccess(Guid Id)
        {
            var accountText = await _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Where(a => a.Id.Equals(Id) && a.AccountId.Equals(CurrentAccount.Id))
                .FirstOrDefaultAsync();

            if (accountText == null)
                return false;

            //Revert access modifier
            accountText.IsPrivate = !accountText.IsPrivate;
            
            _unitOfWork.AccountTexts.UpdateInDatabase(accountText, accountText.Id);
            if (await _unitOfWork.ExecuteUpdatesAsync() > 0)
                return true;

            return false;
        }

        public async Task<bool> DeleteText(Guid Id)
        {
            var accountText = await _unitOfWork.AccountTexts.GetAllFromDatabaseQueryable()
                .Where(a => a.Id.Equals(Id) && a.AccountId.Equals(CurrentAccount.Id))
                .FirstOrDefaultAsync();

            if (accountText == null)
                return false;

            _unitOfWork.AccountTexts.DeleteFromDatabase(accountText);
            if (await _unitOfWork.ExecuteUpdatesAsync() > 0)
                return true;
            
            return false;
        }

        private async Task HandleTexts(AccountText text)
        {
            switch (text.MiningType)
            {
                case ((int)Enumerators.AccountTextMiningType.Entities):
                    var entitites = AylienClient.Instance.Entities(text: text.Text).EntitiesMember;
                    var entityResponse = await entitites.MapToResponseAsync(text, text.Text);
                    _response.EntityTexts.Add(entityResponse);
                    break;
                case ((int)Enumerators.AccountTextMiningType.HashTags):
                    var hashtags = AylienClient.Instance.Hashtags(text: text.Text);
                    var hashtagsResponse = await hashtags.MapToResponseAsync(text, text.Text);
                    _response.HashtagsTexts.Add(hashtagsResponse);
                    break;
                case ((int)Enumerators.AccountTextMiningType.Summarization):
                    var summary = AylienClient.Instance.Summarize(text: text.Text, title: text.Headline, sentencesNumber: text.SentenceNumber.GetValueOrDefault()).Sentences;

                    var summaryResponse = new SummarizationResponse();
                    summaryResponse.Id = text.Id.ToString();
                    summaryResponse.Sentences = summary?.ToList() ?? new List<string>();
                    summaryResponse.Text = text.Text;
                    summaryResponse.AccountDisplayName = $"{text.Account.FirstName} {text.Account.LastName}";
                    summaryResponse.MiningType = text.MiningType;
                    _response.SummarizationTexts.Add(summaryResponse);
                    break;
                default:
                    break;
            }
        }
    }
}
