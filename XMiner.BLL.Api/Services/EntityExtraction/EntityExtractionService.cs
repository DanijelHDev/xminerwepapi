﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Mapping;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;
using XMiner.BLL.Common.Helpers;
using XMiner.Core.DataAccess.Repositories;

namespace XMiner.BLL.Api.Services
{
    public class EntityService : IEntityService
    {
        public async Task<EntityResponse> GetEntities(XMinerRequest request)
        {
            var entities = AylienClient.Instance.Entities(text: request.Text).EntitiesMember;
            
            return await entities.MapToResponseAsync(null, request.Text);
        }
    }
}
