﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Mapping;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;
using XMiner.BLL.Common.Helpers;

namespace XMiner.BLL.Api.Services
{
    public class HashtagExtractionService : IHashtagExtractionService
    {
        public async Task<HashtagsExtractionResponse> GetHashtags(XMinerRequest request)
        {
            var hashtags = AylienClient.Instance.Hashtags(text: request.Text);
            
            return await hashtags.MapToResponseAsync(null, request.Text);
        }
    }
}
