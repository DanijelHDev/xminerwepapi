﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Mapping;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;
using XMiner.BLL.Common.Helpers;
using XMiner.Core.DataAccess.UnitOfWork;

namespace XMiner.BLL.Api.Services.LanguageDetection
{
    public class LanguageDetectionService : ILanguageDetectionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LanguageDetectionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public async Task<LanguageDetectionResponse> DetectLanguage(XMinerRequest request)
        {
            var languageDetection = AylienClient.Instance.Language(text: request.Text);

            return await languageDetection.MapToRepoResponseAsync();
        }
    }
}
