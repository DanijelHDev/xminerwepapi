﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Services
{
    public interface ISettingService
    {
        string GetApiKey();
    }
}
