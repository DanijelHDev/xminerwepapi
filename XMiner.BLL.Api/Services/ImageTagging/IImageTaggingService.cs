﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;

namespace XMiner.BLL.Api.Services.ImageTagging
{
    public interface IImageTaggingService
    {
        ImageTagResponse GetImageTags(XMinerRequest request);
    }
}
