﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;
using XMiner.BLL.Common.Helpers;

namespace XMiner.BLL.Api.Services.Summarization
{
    public class SummarizationService : ISummarizationService
    {
        public SummarizationResponse GetSummarizationResponse(XMinerRequest request, int sentenceNumber)
        {
            var summary = AylienClient.Instance.Summarize(text: request.Text, title: request.Headline, sentencesNumber: sentenceNumber).Sentences;

            return new SummarizationResponse
            {
                Sentences = summary?.ToList() ?? new List<string>(),
                Text = request.Text
            };
        }
    }
}
