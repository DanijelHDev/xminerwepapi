﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Models.Response;

namespace XMiner.BLL.Api.Services.Summarization
{
    public interface ISummarizationService
    {
        SummarizationResponse GetSummarizationResponse(XMinerRequest request, int sentenceCount);
    }
}
