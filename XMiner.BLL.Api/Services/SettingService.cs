﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.UnitOfWork;

namespace XMiner.BLL.Api.Services
{
    public class SettingService : ISettingService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SettingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public string GetApiKey()
        {
            var settings = _unitOfWork.Settings.GetAllFromDatabaseEnumerable().FirstOrDefault();
            return settings != null ? settings.ApiKey : string.Empty;
        }
    }
}
