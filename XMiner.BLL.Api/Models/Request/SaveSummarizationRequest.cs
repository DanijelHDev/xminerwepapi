﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Request
{
    [Serializable]
    public class SaveSummarizationRequest
    {
        public string Text { get; set; }
        public List<string> Sentences { get; set; }
    }
}
