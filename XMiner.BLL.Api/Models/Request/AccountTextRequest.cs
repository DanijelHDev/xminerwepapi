﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Request
{
    public class AccountTextRequest
    {
        [Required]
        public string Text { get; set; }
        [Required, Range(1,3)]
        public short TextMiningType { get; set; }

        public bool IsPrivate { get; set; } = false;

        public string Headline { get; set; }
        public int? SentenceNumber { get; set; }
    }
}
