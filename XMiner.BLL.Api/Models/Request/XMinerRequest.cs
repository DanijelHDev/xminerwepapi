﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Request
{
    public class XMinerRequest
    {
        public string Headline { get; set; }
        public string Text  { get; set; }
        public string Uri { get; set; }
    }
}
