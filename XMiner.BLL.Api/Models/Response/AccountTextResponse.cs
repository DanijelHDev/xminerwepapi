﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Response
{
    public class AccountTextResponse
    {
        public List<EntityResponse> EntityTexts { get; set; } = new List<EntityResponse>();
        public List<SummarizationResponse> SummarizationTexts { get; set; } = new List<SummarizationResponse>();
        public List<HashtagsExtractionResponse> HashtagsTexts { get; set; } = new List<HashtagsExtractionResponse>();
    }
}
