﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Response
{
    public class HashtagsExtractionResponse
    {
        public string Id{ get; set; }
        public string AccountDisplayName { get; set; }
        public int? MiningType { get; set; }
        public string Language { get; set; }
        public List<string> Hashtags { get; set; }
        public string Text { get; set; }
    }
}
