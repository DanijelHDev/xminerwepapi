﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Response
{
    public class ImageTagResponse
    {
        public ImageTags ImageTags { get; set; }
    }

    public class ImageTags
    {
        public string Image { get; set; }
        public List<Tag> Tags { get; set; }
    }

    public class Tag
    {
        public string KeyWord { get; set; }
        public float Confidence { get; set; }
    }

}
