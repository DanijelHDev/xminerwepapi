﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Response
{
    public class SummarizationResponse
    {
        public string Id{ get; set; }
        public string AccountDisplayName { get; set; }
        public int? MiningType { get; set; }
        public List<string> Sentences { get; set; }
        public string Text { get; set; }
    }

}
