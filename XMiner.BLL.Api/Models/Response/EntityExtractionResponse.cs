﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Api.Models.Response
{
    public class EntityResponse
    {
        public string Id{ get; set; }
        public string AccountDisplayName{ get; set; }
        public int? MiningType { get; set; }
        public string Text { get; set; }
        public string Language { get; set; }
        public Entities Entities { get; set; } = new Entities();
    }

    public class Entities
    {
        public List<string> Location { get; set; }
        public List<string> Keyword { get; set; }
        public List<string> Organization { get; set; }
        public List<string> Person { get; set; }
        public List<string> Date { get; set; }
        public List<string> Time { get; set; }
        public List<string> Phone { get; set; }
    }

}
