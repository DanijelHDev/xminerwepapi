﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using XMiner.BLL.Api.Models.Request;

namespace XMiner.BLL.Api.Validation
{
    public class AccountTextValidation : AbstractValidator<AccountTextRequest>
    {
        public AccountTextValidation()
        {
            RuleFor(a => a.Text)
                .NotEmpty()
                .Length(200,int.MaxValue)
                .WithMessage("Text is a required field, with at least 200 characters. ");

            RuleFor(a => a.TextMiningType)
                .NotEmpty()
                .LessThanOrEqualTo((short) 3)
                .WithMessage("Mining type must be lower than or equal to 3. ")
                .GreaterThanOrEqualTo((short) 1)
                .WithMessage("Mining type must be greater than or equal to 1. ");

            RuleFor(a => a.Headline)
                .NotEmpty()
                .When(a => a.TextMiningType.Equals((short) 3));
            
            RuleFor(a => a.SentenceNumber)
                .NotEmpty()
                .GreaterThanOrEqualTo(3)
                .When(a => a.TextMiningType.Equals((short) 3))
                .WithMessage("Sentence number is required. Must be greater than 2. ");
        }
    }
}
