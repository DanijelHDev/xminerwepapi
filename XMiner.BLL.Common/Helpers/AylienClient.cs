﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aylien.TextApi;
using XMiner.Core.Entity.Entities;

namespace XMiner.BLL.Common.Helpers
{
    public static class AylienClient
    {
        private static Client instance;
        public static Client Instance
        {
            get
            {
                if (instance == null)
                    instance = new Client(ApplicationId, ApplicationKey);

                return instance;
            }
        }

        public static string ApplicationId { get; set; }
        public static string ApplicationKey { get; set; }
        
        public static void SetSettings(ServiceProvider serviceProvider)
        {
            ApplicationId = serviceProvider.ApplicationId;
            ApplicationKey = serviceProvider.ApplicationKey;
        }
    }
}
