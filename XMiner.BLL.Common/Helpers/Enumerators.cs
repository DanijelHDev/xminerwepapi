﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Common.Helpers
{
    public class Enumerators
    {
        public enum AccountTextMiningType
        {
            Entities = 1,
            HashTags,
            Summarization,
            ImageTags //OUT OF SCOPE
        }
    }
}
