﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace XMiner.BLL.Common.Helpers
{
    public static class LogHelper
    {
        private static List<ILog> _loggers = new List<ILog>();

        public static ILog Logger(this object obj, object source)
        {
            return GetInstance(obj, source);
        }

        public static ILog Logger(this object obj)
        {
            return GetInstance(obj, null);
        }

        private static ILog GetInstance(object obj, object source)
        {
            var logger = _loggers.Where(l => l.Logger.Name == obj.GetType().FullName).FirstOrDefault();

            if (logger == null)
            {
                logger = LogManager.GetLogger(obj.GetType());
                _loggers.Add(logger);
            }

            if (source != null)
                ThreadContext.Properties["Source"] = source.ToString();

            return logger;
        }
    }
}
