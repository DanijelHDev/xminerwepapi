﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.DatabaseContext;

namespace XMiner.BLL.Common.Helpers
{
    public static class Initializer
    {
        public static void InitializeApiProviderSettings()
        {
            using (var context = new XMinerDbContext())
            {
                var settings = context.ServiceProviders.FirstOrDefault();
                if (settings == null)
                    return;

                AylienClient.SetSettings(settings);
            }
        }
    }
}
