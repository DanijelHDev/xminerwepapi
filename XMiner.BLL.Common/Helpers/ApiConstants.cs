﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Common.Helpers.Constants
{
    public static class ApiConstants
    {
        public static class AylineAuthorization
        {
             public const string AylienApplicationIdHeader = "X-AYLIEN-TextAPI-Application-ID";
             public const string AylienApplicationKeyHeader = "X-AYLIEN-TextAPI-Application-Key";
        }

        public static class ApiUri
        {
            public const string BaseUri = "api/1.0";
        }
    }
}
