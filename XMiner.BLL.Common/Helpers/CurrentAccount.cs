﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace XMiner.BLL.Common.Helpers
{
    public static class CurrentAccount
    {
        public static string Id => Thread.CurrentPrincipal.Identity.GetUserId();

        public static string UserName
        {
            get
            {
                if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                    return Thread.CurrentPrincipal.Identity.GetUserName();
                else
                    return "System";
            }
        }
    }
}
