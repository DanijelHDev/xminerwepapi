﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.BLL.Common.Routes
{
    public static class ApiRoute
    {
        public static class BaseRoute
        {
            public const string BaseUri = "api/1.0";
        }

        public static class AccountTextRoutes
        {
            public const string SaveText = "texts/save";
            public const string GetTexts = "texts";
            public const string GetPublicTexts = "texts/public";
            public const string GetText = "texts/{id}";
            public const string PutText = "texts";
            public const string DeleteText = "texts";
            
            public const string GetTextEntitities = "texts/entities";
            public const string GetTextHashTags = "texts/hashtags";
            public const string GetTextSummarization = "texts/summaries";

        }

        public static class EntityRoutes
        {
            public const string GetEntities = "entities";
        }

        public static class HashtagsRoutes
        {
            public const string GetEntities = "hashtags";
        }

        public static class ImageTaggingRoutes
        {
            public const string GetEntities = "imagetag";
        }

        public static class LanguageDetectionRoutes
        {
            public const string DetectLanguage = "language";
        }


        public static class SummarizationRoutes
        {
            public const string GetSummarization = "summarization/{sentenceCount}";
        }
    }
}
