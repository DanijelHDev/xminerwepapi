﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.DatabaseContext;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.DataAccess.Repositories
{
    public class SettingRepository : BaseRepository<Setting>, ISettingRepository
    {
        public SettingRepository(XMinerDbContext dbContext) : base(dbContext){ }
    }
}
