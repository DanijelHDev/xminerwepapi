﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.DataAccess.Repositories
{
    public interface ISettingRepository : IBaseRepository<Setting>
    {
    }
}
