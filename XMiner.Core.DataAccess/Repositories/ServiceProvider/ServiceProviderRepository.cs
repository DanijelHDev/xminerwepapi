﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.DatabaseContext;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.DataAccess.Repositories
{
    public class ServiceProviderRepository : BaseRepository<ServiceProvider>, IServiceProviderRepository
    {
        public ServiceProviderRepository(XMinerDbContext dbContext) : base(dbContext){}
    }
}
