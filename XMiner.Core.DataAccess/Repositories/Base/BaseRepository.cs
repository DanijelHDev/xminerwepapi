﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.DatabaseContext;
using XMiner.Core.Entity.Common;

namespace XMiner.Core.DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class 
    {
        protected readonly XMinerDbContext DbContext;

        public BaseRepository(XMinerDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IEnumerable<TEntity> GetAllFromDatabaseEnumerable() => DbContext.Set<TEntity>().AsEnumerable();

        public IQueryable<TEntity> GetAllFromDatabaseQueryable() => DbContext.Set<TEntity>().AsQueryable();

        public IEnumerable<TEntity> FindByExpressionInDatabase(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().Where(predicate).AsEnumerable();
        }

        public void AddToDatabase(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public void AddOrUpdateInDatabase(TEntity entity)
        {
            TEntity baseEntity = DbContext.Set<TEntity>().Find(entity);
            if (baseEntity == null)
                AddToDatabase(entity);
            else
                UpdateInDatabase(entity);
        }

        public void AddRangeToDatabase(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().AddRange(entities);
        }

        public void UpdateInDatabase(TEntity entity)
        {
            var baseEntity = DbContext.Set<TEntity>().Find(entity);
            DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);
        }

        public void UpdateInDatabase(TEntity entity, Guid id)
        {
            var baseEntity = DbContext.Set<TEntity>().Find(id);
            DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);
        }

        public void DeleteFromDatabase(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public void DeleteRangeFromDatabase(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().RemoveRange(entities);
        }

        public void ExecuteUpdates()
        {
            DbContext.SaveChanges();
        }

        public async Task ExecuteUpdatesAsync()
        {
            await DbContext.SaveChangesAsync();
        }
    }
}
