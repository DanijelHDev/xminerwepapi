namespace XMiner.Core.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceProviderUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceProviders", "BaseUri", c => c.String(maxLength: 255));
            AddColumn("dbo.ServiceProviders", "ApplicationId", c => c.String(maxLength: 255));
            AddColumn("dbo.ServiceProviders", "ApplicationKey", c => c.String(maxLength: 255));
            DropColumn("dbo.ServiceProviders", "BaseUrl");
            DropColumn("dbo.ServiceProviders", "AccessKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ServiceProviders", "AccessKey", c => c.String(maxLength: 255));
            AddColumn("dbo.ServiceProviders", "BaseUrl", c => c.String(maxLength: 255));
            DropColumn("dbo.ServiceProviders", "ApplicationKey");
            DropColumn("dbo.ServiceProviders", "ApplicationId");
            DropColumn("dbo.ServiceProviders", "BaseUri");
        }
    }
}
