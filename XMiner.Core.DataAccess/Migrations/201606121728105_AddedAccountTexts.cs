namespace XMiner.Core.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAccountTexts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountTexts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AccountId = c.String(nullable: false, maxLength: 128),
                        Text = c.String(),
                        MiningType = c.Int(nullable: false),
                        Headline = c.String(maxLength: 255),
                        SentenceNumber = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        ModifiedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountTexts", "AccountId", "dbo.Accounts");
            DropIndex("dbo.AccountTexts", new[] { "AccountId" });
            DropTable("dbo.AccountTexts");
        }
    }
}
