namespace XMiner.Core.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIdentity : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Accounts");
            CreateTable(
                "dbo.AccountRole",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AccountUserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Account_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AccountRole", t => t.RoleId)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.RoleId)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.AccountClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        Account_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.AccountLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Account_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            AddColumn("dbo.Accounts", "FirstName", c => c.String());
            AddColumn("dbo.Accounts", "LastName", c => c.String());
            AddColumn("dbo.Accounts", "Email", c => c.String());
            AddColumn("dbo.Accounts", "EmailConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Accounts", "PasswordHash", c => c.String());
            AddColumn("dbo.Accounts", "SecurityStamp", c => c.String());
            AddColumn("dbo.Accounts", "PhoneNumber", c => c.String());
            AddColumn("dbo.Accounts", "PhoneNumberConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Accounts", "TwoFactorEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Accounts", "LockoutEndDateUtc", c => c.DateTime());
            AddColumn("dbo.Accounts", "LockoutEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Accounts", "AccessFailedCount", c => c.Int(nullable: false));
            AddColumn("dbo.Accounts", "UserName", c => c.String());
            AlterColumn("dbo.Accounts", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Accounts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountUserRole", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.AccountLogin", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.AccountClaim", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.AccountUserRole", "RoleId", "dbo.AccountRole");
            DropIndex("dbo.AccountLogin", new[] { "Account_Id" });
            DropIndex("dbo.AccountClaim", new[] { "Account_Id" });
            DropIndex("dbo.AccountUserRole", new[] { "Account_Id" });
            DropIndex("dbo.AccountUserRole", new[] { "RoleId" });
            DropIndex("dbo.AccountRole", "RoleNameIndex");
            DropPrimaryKey("dbo.Accounts");
            AlterColumn("dbo.Accounts", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Accounts", "UserName");
            DropColumn("dbo.Accounts", "AccessFailedCount");
            DropColumn("dbo.Accounts", "LockoutEnabled");
            DropColumn("dbo.Accounts", "LockoutEndDateUtc");
            DropColumn("dbo.Accounts", "TwoFactorEnabled");
            DropColumn("dbo.Accounts", "PhoneNumberConfirmed");
            DropColumn("dbo.Accounts", "PhoneNumber");
            DropColumn("dbo.Accounts", "SecurityStamp");
            DropColumn("dbo.Accounts", "PasswordHash");
            DropColumn("dbo.Accounts", "EmailConfirmed");
            DropColumn("dbo.Accounts", "Email");
            DropColumn("dbo.Accounts", "LastName");
            DropColumn("dbo.Accounts", "FirstName");
            DropTable("dbo.AccountLogin");
            DropTable("dbo.AccountClaim");
            DropTable("dbo.AccountUserRole");
            DropTable("dbo.AccountRole");
            AddPrimaryKey("dbo.Accounts", "Id");
        }
    }
}
