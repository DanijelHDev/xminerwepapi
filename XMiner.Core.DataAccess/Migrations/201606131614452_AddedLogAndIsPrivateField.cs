namespace XMiner.Core.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLogAndIsPrivateField : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(),
                        Level = c.String(),
                        Logger = c.String(),
                        Message = c.String(maxLength: 255),
                        Exception = c.String(),
                        Source = c.String(maxLength: 255),
                        App = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AccountTexts", "IsPrivate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountTexts", "IsPrivate");
            DropTable("dbo.Logs");
        }
    }
}
