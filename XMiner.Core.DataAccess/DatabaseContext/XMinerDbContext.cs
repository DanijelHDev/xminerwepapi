﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using XMiner.Core.Entity.Entities;
using XMiner.Core.Entity.EntityConfiguration;

namespace XMiner.Core.DataAccess.DatabaseContext
{
    public class XMinerDbContext : IdentityDbContext<Account>
    {
        public XMinerDbContext() : base("XMinerDbContext", throwIfV1Schema: false)
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
            #if DEBUG
                this.Database.Log = s => Debug.Write(s);
            #endif
        }

        //public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountText> AccountTexts { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<Setting> Settings { get; set; }

        //Fluent Api
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /* ----------------------
                Owin/Katana Identity 
            -------------------------*/
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>().ToTable("Account");
            modelBuilder.Entity<IdentityRole>().ToTable("AccountRole");
            modelBuilder.Entity<IdentityUserRole>().ToTable("AccountUserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AccountClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("AccountLogin");

            /* Indexes http://gavindraper.com/2014/06/26/entity-framework-fluent-api-and-indexing/ */

            //Entity configurations
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new AccountEntityConfiguration());
            modelBuilder.Configurations.Add(new AccountTextEntityConfiguration());
            modelBuilder.Configurations.Add(new SettingEntityConfiguration());
            modelBuilder.Configurations.Add(new ServiceProviderEntityConfiguration());
            modelBuilder.Configurations.Add(new LogEntityConfiguration());
        }

        public static XMinerDbContext Create()
        {
            return new XMinerDbContext();
        }
    }
}
