﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.DatabaseContext;
using XMiner.Core.DataAccess.Repositories;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly XMinerDbContext _dbContext;

        //TODO: initialize repositories and pass context
        //...
        public UnitOfWork()
        {
            _dbContext = new XMinerDbContext();

            AccountTexts = new AccountTextRepository(_dbContext);
            Settings = new SettingRepository(_dbContext);
            ServiceProviders = new ServiceProviderRepository(_dbContext);
        }

        public int ExecuteUpdates()
        {
            return _dbContext.SaveChanges();
        }

        public async Task<int> ExecuteUpdatesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        //TODO: Define IRepository Resources
        //...
        public IAccountTextRepository AccountTexts { get; private set; }
        public ISettingRepository Settings { get; private set; }
        public IServiceProviderRepository ServiceProviders { get; private set; }
    }
}
