﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.DataAccess.Repositories;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.DataAccess.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IAccountTextRepository AccountTexts { get; }
        ISettingRepository Settings { get; }
        IServiceProviderRepository ServiceProviders { get; }

        int ExecuteUpdates();
        Task<int> ExecuteUpdatesAsync();
    }
}
