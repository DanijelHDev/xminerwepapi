﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.Core.Entity.Common
{
    public abstract class AuditableEntity<T> : Entity<T>, IAuditableEntity
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        [MaxLength(256)]
        public string CreatedBy{ get; set; }
        [MaxLength(256)]
        public string ModifiedBy { get; set; }
    }
}
