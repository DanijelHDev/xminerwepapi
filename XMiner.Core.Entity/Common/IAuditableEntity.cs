﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMiner.Core.Entity.Common
{
    public interface IAuditableEntity
    {
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }

        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
    }
}
