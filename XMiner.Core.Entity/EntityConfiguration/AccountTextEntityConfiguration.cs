﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.Entity.EntityConfiguration
{
    public class AccountTextEntityConfiguration : EntityTypeConfiguration<AccountText>
    {
        public AccountTextEntityConfiguration()
        {
            //ID
            HasKey(x => x.Id);

            //PROPERTIES
            Property(x => x.Text).IsMaxLength();
            Property(x => x.Headline).HasMaxLength(255);

            //ONE TO MANY
            HasRequired(x => x.Account)
                .WithMany(a => a.AccountTexts)
                .HasForeignKey(x => x.AccountId);
        }
    }
}
