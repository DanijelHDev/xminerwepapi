﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.Entity.EntityConfiguration
{
    public class SettingEntityConfiguration : EntityTypeConfiguration<Setting>
    {
        public SettingEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.ApiKey).HasMaxLength(255);
        }
    }
}
