﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.Entity.EntityConfiguration
{
    public class LogEntityConfiguration : EntityTypeConfiguration<Log>
    {
        public LogEntityConfiguration()
        {
            HasKey(l => l.Id);

            //PROPERTIES
            Property(l => l.Source).HasMaxLength(255);
            Property(l => l.Message).HasMaxLength(255);
        }
    }
}
