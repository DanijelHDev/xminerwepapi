﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Entities;

namespace XMiner.Core.Entity.EntityConfiguration
{
    public class ServiceProviderEntityConfiguration : EntityTypeConfiguration<ServiceProvider>
    {
        public ServiceProviderEntityConfiguration()
        {
            HasKey(x => x.Id);

            //PROPERTIES
            Property(x => x.Name).HasMaxLength(255);
            Property(x => x.BaseUri).HasMaxLength(255);
            Property(x => x.ApplicationId).HasMaxLength(255);
            Property(x => x.ApplicationKey).HasMaxLength(255);
        }

    }
}
