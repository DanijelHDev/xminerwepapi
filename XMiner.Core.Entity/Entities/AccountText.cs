﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Common;

namespace XMiner.Core.Entity.Entities
{
    public class AccountText : AuditableEntity<Guid>
    {
        public string AccountId { get; set; }
        public virtual Account Account { get; set; }

        public bool IsPrivate { get; set; } = false;

        public string Text{ get; set; }
        public int MiningType { get; set; }

        public string Headline { get; set; }
        public int? SentenceNumber { get; set; }
    }


}
