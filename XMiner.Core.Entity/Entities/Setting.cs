﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Common;

namespace XMiner.Core.Entity.Entities
{
    public class Setting : Entity<int>
    {
        public string ApiKey { get; set; }
    }
}
