﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMiner.Core.Entity.Common;

namespace XMiner.Core.Entity.Entities
{
    public class ServiceProvider : AuditableEntity<short>
    {
        public string Name { get; set; }
        public string BaseUri { get; set; }
        public string ApplicationId { get; set; }
        public string ApplicationKey { get; set; } 
    }
}
