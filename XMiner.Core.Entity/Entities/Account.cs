﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using XMiner.Core.Entity.Common;

namespace XMiner.Core.Entity.Entities
{
    public class Account : IdentityUser
    {
        public string FirstName{ get; set; }

        public string LastName { get; set; }

        public virtual ICollection<AccountText> AccountTexts { get; set; } = new List<AccountText>();

        #region Auditable Properties
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        [MaxLength(256)]
        public string CreatedBy { get; set; }
        [MaxLength(256)]
        public string ModifiedBy { get; set; }
        #endregion

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Account> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
