﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(XMiner.App.Api.Startup))]

namespace XMiner.App.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var config = GlobalConfiguration.Configuration;

            //config
            //.EnableSwagger(c => c.SingleApiVersion("v1", "A title for your API"))
            //.EnableSwaggerUi();
        }
    }
}
