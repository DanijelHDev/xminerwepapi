﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services.ImageTagging;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    /// <summary>
    /// OUT OF SCOPE
    /// </summary>
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class ImageTaggingController : ApiController
    {
        private readonly IImageTaggingService _imageTaggingService;

        public ImageTaggingController(IImageTaggingService imageTaggingService)
        {
            _imageTaggingService = imageTaggingService;
        }

        [HttpPost, Route(ApiRoute.ImageTaggingRoutes.GetEntities)]
        public IHttpActionResult GetImageTags([FromBody] XMinerRequest request)
        {
            try
            {
                return Ok();

                if (String.IsNullOrEmpty(request?.Uri))
                    return BadRequest();

                var result = _imageTaggingService.GetImageTags(request);

                if (result == null) return InternalServerError();

                return Ok(result);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
