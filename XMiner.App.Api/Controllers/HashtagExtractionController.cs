﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Helpers.Constants;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class HashtagExtractionController : ApiController
    {
        private readonly IHashtagExtractionService _hashtagExtractionService;
        public HashtagExtractionController(IHashtagExtractionService hashtagExtractionService)
        {
            _hashtagExtractionService = hashtagExtractionService;
        }

        [HttpPost, Route(ApiRoute.HashtagsRoutes.GetEntities)]
        public async Task<IHttpActionResult> HandleHashtags([FromBody] XMinerRequest request)
        {
            try
            {
                if (request?.Text?.Length < 200)
                    return BadRequest();

                var response = await _hashtagExtractionService.GetHashtags(request);
                if (response == null)
                    return InternalServerError();
                
                return Ok(response);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
