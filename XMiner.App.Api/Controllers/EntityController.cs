﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Helpers.Constants;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class EntityController : ApiController
    {
        private readonly IEntityService _entityService;
        public EntityController(IEntityService entityService)
        {
            _entityService = entityService;
        }

        [HttpPost, Route(ApiRoute.EntityRoutes.GetEntities)]
        public async Task<IHttpActionResult> HandleEntities([FromBody] XMinerRequest request)
        {
            try
            {
                if (request == null || request?.Text?.Length < 200)
                    return BadRequest();
                
                var result = await _entityService.GetEntities(request);

                if (result == null) return InternalServerError();
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
