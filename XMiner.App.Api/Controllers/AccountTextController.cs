﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FluentValidation.Results;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services;
using XMiner.BLL.Api.Validation;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    [Authorize]
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class AccountTextController : ApiController
    {
        private readonly IAccountTextService _accountTextService;

        public AccountTextController(IAccountTextService accountTextService)
        {
            _accountTextService = accountTextService;
        }

        /// <summary>
        /// Get public texts
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route(ApiRoute.AccountTextRoutes.GetPublicTexts)]
        public async Task<IHttpActionResult> GetAllPublicTexts()
        {
            try
            {
                var accountTexts = await _accountTextService.GetAllPublicTexts();
                return Ok(accountTexts);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get single text
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route(ApiRoute.AccountTextRoutes.GetText)]
        public async Task<IHttpActionResult> GetText([FromUri] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return BadRequest();

                var response = await _accountTextService.GetText(id);

                return Ok(response);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get texts
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route(ApiRoute.AccountTextRoutes.GetTexts)]
        public async Task<IHttpActionResult> GetTexts()
        {
            try
            {
                var response = await _accountTextService.GetTexts();

                return Ok(response);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Save account texts
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, Route(ApiRoute.AccountTextRoutes.SaveText)]
        public async Task<IHttpActionResult> PostSaveText([FromBody] AccountTextRequest request)
        {
            try
            {
                AccountTextValidation validator = new AccountTextValidation();
                var results = validator.Validate(request);
                if (!results.IsValid)
                    return BadRequest(String.Join(String.Empty, results.Errors));

                if (await _accountTextService.SaveText(request))
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
        
        [HttpGet, Route(ApiRoute.AccountTextRoutes.GetTextEntitities)]
        public async Task<IHttpActionResult> GetTextEntitities()
        {
            try
            {
                var response = await _accountTextService.GetTextsEntities();

                return Ok(response);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Update texts -> If public, then private, if private then public
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut, Route(ApiRoute.AccountTextRoutes.PutText)]
        public async Task<IHttpActionResult> UpdatePublicPrivate([FromUri] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return BadRequest();

                if (!(await _accountTextService.SetAccountTextAccess(id)))
                    return NotFound();

                return Ok();
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }

        [HttpDelete, Route(ApiRoute.AccountTextRoutes.DeleteText)]
        public async Task<IHttpActionResult> DeleteText([FromUri] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return BadRequest();

                if (!(await _accountTextService.DeleteText(id)))
                    return NotFound();

                return Ok();
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
