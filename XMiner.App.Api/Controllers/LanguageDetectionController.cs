﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services.LanguageDetection;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class LanguageDetectionController : ApiController
    {
        private readonly ILanguageDetectionService _languageDetectionService;

        public LanguageDetectionController(ILanguageDetectionService languageDetectionService)
        {
            _languageDetectionService = languageDetectionService;
        }

        [HttpPost, Route(ApiRoute.LanguageDetectionRoutes.DetectLanguage)]
        public async Task<IHttpActionResult> DetectLanguage([FromBody] XMinerRequest request)
        {
            try
            {
                if (request?.Text?.Length < 200)
                    return BadRequest();
                
                var languageDetection = await _languageDetectionService.DetectLanguage(request);
                if (languageDetection == null)
                    return InternalServerError();

                return Ok(languageDetection);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
