﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XMiner.BLL.Api.Services;
using XMiner.BLL.Common.Helpers;

namespace XMiner.App.Api.Controllers
{
    [Authorize]
    public class ApiInfoController : BaseController
    {
        private readonly ISettingService _settingService;
        public ApiInfoController(ISettingService settingService)
        {
            this._settingService = settingService;
        }

        [HttpGet, Route("settings")]
        public IHttpActionResult GetApiKey()
        {
            try
            {
                var apiKey = _settingService.GetApiKey();
                return Ok(apiKey);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
