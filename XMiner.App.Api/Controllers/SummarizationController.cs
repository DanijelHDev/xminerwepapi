﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XMiner.BLL.Api.Models.Request;
using XMiner.BLL.Api.Services.Summarization;
using XMiner.BLL.Common.Helpers;
using XMiner.BLL.Common.Routes;

namespace XMiner.App.Api.Controllers
{
    [RoutePrefix(ApiRoute.BaseRoute.BaseUri)]
    public class SummarizationController : ApiController
    {
        private readonly ISummarizationService _summarizationService;

        public SummarizationController(ISummarizationService summarizationService)
        {
            _summarizationService = summarizationService;   
        }

        [HttpPost, Route(ApiRoute.SummarizationRoutes.GetSummarization)]
        public IHttpActionResult Summarization([FromUri] int sentenceCount, [FromBody] XMinerRequest request)
        {
            try
            {
                if (request?.Text == null || request?.Headline == null || request?.Text?.Length < 200)
                    return BadRequest();

                var result = _summarizationService.GetSummarizationResponse(request, sentenceCount);

                if (result == null) return InternalServerError();

                return Ok(result);
            }
            catch (Exception ex)
            {
                this.Logger(CurrentAccount.UserName).Error(ex.Message, ex);
                return InternalServerError(ex);
            }
        }
    }
}
