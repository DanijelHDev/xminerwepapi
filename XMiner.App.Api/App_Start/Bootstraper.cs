﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using XMiner.App.Api.DIModules;

namespace XMiner.App.Api.App_Start
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            SetUpAutofacWebApi();
        }

        public static void SetUpAutofacWebApi()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());//.PropertiesAutowired();

            //Register Dependencies
            builder.RegisterModule(new DIServiceModule());
            builder.RegisterModule(new DIUnitOfWorkModule());

            var container = builder.Build();

            // Setup the dependency resolver
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}