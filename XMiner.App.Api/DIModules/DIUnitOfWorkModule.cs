﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using XMiner.Core.DataAccess.UnitOfWork;

namespace XMiner.App.Api.DIModules
{
    public class DIUnitOfWorkModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerLifetimeScope();
        }
    }
}